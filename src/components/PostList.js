import React, { useState, useEffect } from "react";
import "./../App.css";
import LoaderComponent from "./Loader";
import { POST_URL } from "./../constants";

export default function PostList(props) {
  const [state, setPosts] = useState({
    posts: [],
    error: false,
    isLoading: false,
  });

  useEffect(async () => {
    try {
      setPosts({ ...state, isLoading: true });
      const response = await fetch(POST_URL);
      const posts = await response.json();
      setPosts({ ...state, isLoading: false, posts: posts });
    } catch (err) {
      setPosts({ ...state, isLoading: false, error: true });
    }
  }, []);

  return (
    <div className="post-container">
      {state.isLoading && (
        <div className="loading">
          <LoaderComponent />
        </div>
      )}
      {state.error && <p className="error">Error while loading posts</p>}
      <div className="posts-section">
        {state.posts.length > 0 && (
          <div className="recentPost">
            <p>Top 10 posts.</p>
          </div>
        )}
        {state.posts.map((post) => {
          return (
            <div key={post.id} className="single-item">
              <div className="post-top">
                <p className="post-title">{post.title}</p>
                <p className="post-user">{props.user.name}</p>
              </div>
              <p className="post-description">{post.body}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
