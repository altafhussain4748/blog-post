import React, { useState } from "react";
import "./../App.css";
import LoaderComponent from "./Loader";
import { CREATE_POST } from "./../constants";

export default function CreatePost(props) {
  const [state, setForm] = useState({
    isLoading: false,
    error: false,
    title: "",
    body: "",
    msg: "",
  });

  function handleType(e) {
    setForm({ ...state, [e.target.name]: e.target.value });
  }

  async function onSubmit(e) {
    setForm({ ...state, isLoading: true });
    e.preventDefault();
    const res = await fetch(CREATE_POST, {
      method: "POST",
      body: JSON.stringify({
        title: state.title,
        body: state.body,
        userId: props.id,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
    const result = await res.json();
    setForm({
      ...state,
      isLoading: false,
      msg: "Post created successfully.",
    });
  }

  return (
    <div className="form-container">
      {state.isLoading && (
        <div className="loading">
          <LoaderComponent />
        </div>
      )}
      <form onSubmit={(e) => onSubmit(e)}>
        {state.msg && <p className="message">{state.msg}</p>}
        <p>Create New Post</p>
        <div className="form-group">
          <label>Title:</label>
          <input
            type="text"
            name="title"
            className="form-element"
            onChange={(e) => handleType(e)}
          />
        </div>
        <div className="form-group">
          <label>Body:</label>
          <textarea
            name="body"
            id="body"
            cols="30"
            rows="10"
            className="form-element"
            onChange={(e) => handleType(e)}
          ></textarea>
        </div>
        <input type="submit" value="Submit" className="btn" />
      </form>
    </div>
  );
}
